## To build the app you need a mac computer, because SDK is not available on other platforms.  
* Install “XCode”  
XCode can be installed from AppStore and is free  

* Clone project repository  
```
SSH: git clone git@gitlab.com:avainola/ITX8540-iOS-App.git
```
```
HTTPS: git clone https://gitlab.com/avainola/ITX8540-iOS-App.git
```  

* Clone Charts submodule
``` 
git submodule update --init --recursive
``` 

* Open the project file (Mediag.xcodeproj) found in the project root directory  

* Select a target  
If you have connected a device, it should be available in the target list  
It is also possible to run the app in a Simulator  
NB! App is desinged for use on 9,7” iPad Pro.

* Press the build (▶︎) button  
NB! When building on iPad, make sure that the Charts submodule build settings have Base SDK set to Latest iOS (10.1)