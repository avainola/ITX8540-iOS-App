//
//  ViewController.swift
//  Mediag
//
//  Created by Artur Vainola on 01/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    @IBOutlet weak var patientId: UITextField!
    @IBOutlet weak var start: UIButton!
    
    @IBAction func patientIdChanged(_ sender: UITextField) {
        patientId.backgroundColor = UIColor.white
    }
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
    
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(notification: Notification){
        self.topConstraint.constant /= 2
    }
    
    func keyboardWillBeHidden(notification: Notification){
        self.topConstraint.constant *= 2
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (!patientId.hasText) {
            patientId.backgroundColor = UIColor(red: 1.0, green: 0.9, blue: 0.9, alpha: 1.0)
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {		
        if let infovc = segue.destination as? InfoViewController {
            infovc.patientId = self.patientId.text!
            infovc.persistenceManager = PersistenceManager(patientId: self.patientId.text!)
        }
        self.patientId.text = ""
    }

}

