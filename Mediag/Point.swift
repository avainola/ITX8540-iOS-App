//
//  Point.swift
//  Mediag
//
//  Created by Artur Vainola and Konstantin Bardõš on 03/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit

struct Point /*: PropertyListReadable*/ {
    let location: CGPoint
    let force: CGFloat
	let altitudeAngle: CGFloat
	let azimuthAngle : CGFloat
    let timestamp: TimeInterval
    
    init(location: CGPoint, force: CGFloat, altitudeAngle: CGFloat,
        azimuthAngle : CGFloat, timestamp: TimeInterval) {
        self.location = location
        self.force = force
        self.altitudeAngle = altitudeAngle
        self.azimuthAngle = azimuthAngle
        self.timestamp = timestamp
    }
    
    init?(deserializedData: [String: Double]) {
        let loc = CGPoint(x: Double(deserializedData["x"]!), y: Double(deserializedData["y"]!))
        location = loc
        force = CGFloat(deserializedData["force"]!)
        altitudeAngle = CGFloat(deserializedData["altang"]!)
        azimuthAngle = CGFloat(deserializedData["aziang"]!)
        timestamp = TimeInterval(deserializedData["time"]!)
    }
	
    func getSerializableData() -> [String: Double] {
        return [
            "x": Double(location.x),
            "y": Double(location.y),
            "force": Double(force),
            "altang": Double(altitudeAngle),
            "aziang": Double(azimuthAngle),
            "time": Double(timestamp)
        ]
    }

}
