//
//  DrawView.swift
//  Mediag
//
//  Created by Artur Vainola and Konstantin Bardõš on 03/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.

import UIKit

enum DrawingMode {
    case Lines
    case Dots
    case Pressure
}

protocol DrawViewDelegate {
    func drawingStarted(drawView: DrawView)
}

class DrawView: UIView {
    
    var delegate: DrawViewDelegate?
    var patientId: String!
	var isAdvancedMode = true
    private var readOnly = false
	var isFingerInput = true
	var isFingerInputRestricted = true
    private var drawing: Drawing?
    private var line = [Point]()
	private var drawingMode = DrawingMode.Lines
	
	var backgroundFileName : String!
	var backgroundDrawingMode : DrawingMode!
	var type: String!
    var hand: String!
	
	var drawingModeIndex : Int {
		get {
			switch  drawingMode {
			case .Dots: return 0
			case .Lines: return 1
			case .Pressure: return 2
			}
		}
		
		set (newIndex) {
            print(newIndex)
			switch newIndex {
			case 0: drawingMode = DrawingMode.Dots
			case 1: drawingMode = DrawingMode.Lines
			case 2: drawingMode = DrawingMode.Pressure
			default: drawingMode = DrawingMode.Lines
			}
			
			self.setNeedsDisplay()
		}
	}
	
	private var msg : String!
	private var timeStart: Date!
	private var timeCurrent: Date!
	private var pointsNumberTotal = 0
	private var pointsNumberCurrentStroke = 0
	private var textView = UITextView(frame: CGRect(x: 30, y: 80, width: 200, height: 200))
	private var imageBackground : UIImageView!
	private var imageTemp : UIImageView!
	private var warningLabel : UILabel!
	
	private var currentCGPoint : CGPoint!
	private var prevCGPoint1 : CGPoint!
	private var prevCGPoint2 : CGPoint!
	
	private var widthForDot : CGFloat = 5.0
	private var widthForLine : CGFloat = 5.0
	private var widthForPressure : CGFloat = 2.0
	
	var widthScaling : CGFloat = 1.0
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		imageBackground = UIImageView()
		imageBackground.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
		
		imageTemp = UIImageView()
		imageTemp.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
		
		warningLabel = UILabel()
		warningLabel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
		warningLabel.font = UIFont(name: "HelveticaNeue-UltraLight", size: 60)
		warningLabel.textAlignment = NSTextAlignment.center
		warningLabel.backgroundColor = UIColor.white
		warningLabel.alpha = 0.9
		warningLabel.text = "Please use ApplePencil ✏️ for drawing!"
		warningLabel.isHidden = true
		
		self.addSubview(imageBackground)
		self.addSubview(imageTemp)
		self.addSubview(warningLabel)
		
		if isAdvancedMode {
			self.addSubview(textView)
			textView.font = UIFont(name: "Menlo", size: 9)
			textView.backgroundColor = UIColor.clear
			textView.isHidden = true
		}
	}
	
	func getBackgroundDrawingMode() -> DrawingMode {
		switch type {
		case "poppelreuter":	return DrawingMode.Dots
		case "sine_copy":		return DrawingMode.Lines
		default:				return DrawingMode.Lines
		}
	}
	
	override func layoutSubviews() {
		if let currentType = type {
			if PersistenceManager.existsPreloadedFile(name: currentType) {
				drawBackground(name: currentType, drawingMode: getBackgroundDrawingMode())
			}
		}
	}
	
	func drawBackground(name: String, drawingMode: DrawingMode) {
		
		imageBackground.image = nil
		imageBackground.alpha = 0.27
		
		let context = imageContextStart(image: imageBackground)
		
		switch drawingMode {
		case .Dots		: drawDots			(context: context!, drawing: PersistenceManager.getPreloadedDrawing(name: name))
		case .Lines		: drawStraightLines	(context: context!, drawing: PersistenceManager.getPreloadedDrawing(name: name))
		default			: drawStraightLines	(context: context!, drawing: PersistenceManager.getPreloadedDrawing(name: name))
		}
		
		imageContextEnd(image: imageBackground)
	}
	
    
    func getDrawing() -> Drawing? {
        return drawing
    }
    
    func setDrawing(_drawing: Drawing) {
        self.drawing = _drawing
        self.setNeedsDisplay()
    }
    
    func disableEdit() {
        self.readOnly = true
    }
    
    func setDrawingMode(mode: DrawingMode) {
        drawingMode = mode
    }
    

    func endLineWith(point: Point) {
        line.append(point)
        drawing?.data.append(line)
        line.removeAll()
    }
    
    func clear() {
        line.removeAll()
        drawing?.data.removeAll()
		pointsNumberTotal = 0
		imageTemp.image = nil
    }
    
    private func getPointFromTouch(touch: UITouch) -> Point {
        return Point(location: touch.location(in: self),
                     force: touch.force,
                     altitudeAngle: touch.altitudeAngle,
                     azimuthAngle: touch.azimuthAngle(in: self),
                     timestamp: Date.timeIntervalSinceReferenceDate)
    }
	

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		if readOnly { return }
		
		if let touch = touches.first {
			
			if touch.type == UITouchType.stylus {
				isFingerInput = false
			} else {
				isFingerInput = true
			}
			
			if !isFingerInput || !isFingerInputRestricted {
				if drawing == nil {
					self.delegate?.drawingStarted(drawView: self)
                    drawing = Drawing(_patientId: patientId, _type: type, _hand: hand)
				}
				timeStart = Date()
				line.append(getPointFromTouch(touch: touch))
				prevCGPoint1 = touch.previousLocation(in: self)
				prevCGPoint2 = touch.previousLocation(in: self)
				pointsNumberTotal += 1
				pointsNumberCurrentStroke = 0
			} else {
				warningLabel.isHidden = false
			}
			
		}
	}
	
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		if readOnly { return }
		if !isFingerInput || !isFingerInputRestricted {
			if let touch = touches.first {
				let currentPoint = getPointFromTouch(touch: touch)
				line.append(currentPoint)
				
				currentCGPoint = currentPoint.location
				prevCGPoint2 = prevCGPoint1
				prevCGPoint1 = touch.previousLocation(in: self)
				
				// print("points \(currentPoint.x) \(prevPoint1.x) \(prevPoint2.x)")
				// Incremental drawing
				
				let context = imageContextStart(image: imageTemp)
				switch self.drawingMode {
				case .Lines:
					drawSingleLine(context: context!,
					               currentPoint: currentCGPoint,
					               prevPoint1: prevCGPoint1,
					               prevPoint2: prevCGPoint2,
					               width : widthForLine + 2.0,
					               colorRed: 0.0,
					               colorGreen: 0.0,
					               colorBlue: 0.0)
				case .Dots:
					drawSingleDot(context: context!,
					              currentPoint: currentCGPoint,
					              width : widthForDot + 2.0,
					              colorRed: 1.0,
					              colorGreen: 0.0,
					              colorBlue: 0.0)
				case .Pressure:
					drawSingleLine(context: context!,
					               currentPoint: currentCGPoint,
					               prevPoint1: prevCGPoint1,
					               prevPoint2: prevCGPoint2,
					               width : widthForPressure + 2.0 + currentPoint.force * 20.0,
					               colorRed: 1.0 / (1.0 + pow(5.0, (-currentPoint.force + 1))),
					               colorGreen: 0.0,
					               colorBlue: 0.0)
				}
				imageContextEnd(image: imageTemp)
				
				if isAdvancedMode {
					let location = touch.location(in: self)
					let force = touch.force
					let touchType = touch.type.rawValue
					pointsNumberTotal += 1
					pointsNumberCurrentStroke += 1
					timeCurrent = Date()
					let timeElapsed  = Float(timeCurrent.timeIntervalSince(timeStart as Date))
					let currentSpeed = Float(pointsNumberCurrentStroke) / timeElapsed
					msg = "Current Point: \n" +
						
						"currentX:    \(String(format: "%.2f", location.x))  \n" +
						"currentY:    \(String(format: "%.2f", location.y))  \n" +
						"force:       \(String(format: "%.2f", force))  \n" +
						"angleAlt:    \(String(format: "%.2f", touch.altitudeAngle)) rad\n" +
						"angleAzim:   \(String(format: "%.2f", touch.azimuthAngle(in: self))) rad\n\n" +
						"touchType:   \(String(format: "%.0f", touchType))  \n" +
						
						"points:      \(pointsNumberCurrentStroke) points\n" +
						"timeElapsed: \(String(format: "%0.2f", timeElapsed)) sec\n" +
						"speed:       \(String(format: "%.2f", currentSpeed)) points/sec " +
					"pointsTotal: \(pointsNumberTotal) points\n"
					textView.isHidden = false
					textView.text = msg
				}
			}
		}
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		if readOnly { return }
		
		if !isFingerInput || !isFingerInputRestricted {
			if let touch = touches.first {
				endLineWith(point: getPointFromTouch(touch: touch));
			}
		}
		
		isFingerInput = true
		warningLabel.isHidden = true
		textView.isHidden = true
		setNeedsDisplay()
	}
	

	private func drawSingleLine(context: CGContext,
	                            currentPoint: CGPoint,
	                            prevPoint1: CGPoint,
	                            prevPoint2: CGPoint,
	                            width: CGFloat,
	                            colorRed: CGFloat,
	                            colorGreen: CGFloat,
	                            colorBlue: CGFloat) {
		
		let mid1 = CGPoint(x: (prevPoint1.x + prevPoint2.x)*0.5, y: (prevPoint1.y + prevPoint2.y)*0.5)
		let mid2 = CGPoint(x: (currentPoint.x + prevPoint1.x)*0.5, y: (currentPoint.y + prevPoint1.y)*0.5)
		context.move(to: mid1)
		context.addQuadCurve(to: mid2, control: prevPoint1)
		context.setLineWidth(width * widthScaling)
		context.setLineCap(CGLineCap.round)
		context.setStrokeColor(red: colorRed, green: colorGreen, blue: colorBlue,alpha: 1.0)
		context.setBlendMode(CGBlendMode.normal)
		context.strokePath()
	}
	
	private func drawSingleStraightLine(context: CGContext,
	                                    currentPoint: CGPoint,
	                                    prevPoint: CGPoint,
	                                    width: CGFloat,
	                                    colorRed: CGFloat,
	                                    colorGreen: CGFloat,
	                                    colorBlue: CGFloat) {
		
		context.move(to: prevPoint)
		context.addLine(to: currentPoint)
		context.setLineWidth(width * widthScaling)
		context.setLineCap(CGLineCap.round)
		context.setStrokeColor(red: colorRed, green: colorGreen, blue: colorBlue,alpha: 1.0)
		context.setBlendMode(CGBlendMode.normal)
		context.strokePath()
	}
	
	private func drawSingleDot(context: CGContext,
	                           currentPoint: CGPoint,
	                           width: CGFloat,
	                           colorRed: CGFloat,
	                           colorGreen: CGFloat,
	                           colorBlue: CGFloat) {
		
		
		context.move(to: currentPoint)
		context.addLine(to: currentPoint)
		context.setLineWidth(width * widthScaling)
		context.setLineCap(CGLineCap.round)
		context.setStrokeColor(red: colorRed, green: colorGreen, blue: colorBlue,alpha: 1.0)
		
		context.setBlendMode(CGBlendMode.normal)
		context.strokePath()
	}
	
	
	func drawDots(context: CGContext, drawing: Drawing?) {
		if drawing == nil {
			return
		}
		for line in (drawing?.data)! {
			for point in line {
				drawSingleDot(context: context,
				              currentPoint: point.location,
				              width: widthForDot,
				              colorRed: 0.0,
				              colorGreen: 0.0,
				              colorBlue: 0.0)
			}
		}
		for point in line {
			drawSingleDot(context: context,
			              currentPoint: point.location,
			              width: widthForDot,
			              colorRed: 0.0,
			              colorGreen: 0.0,
			              colorBlue: 0.0)
		}
	}
	
	func drawStraightLines(context: CGContext, drawing: Drawing?) {
		if drawing == nil {
			return
		}
		if line.count > 0 {
			for i in 1..<(line.count) {
				drawSingleStraightLine(context: context,
				                       currentPoint: line[i].location,
				                       prevPoint: line[i-1].location,
				                       width: widthForLine,
				                       colorRed: 0.0,
				                       colorGreen: 0.0,
				                       colorBlue: 0.0)
			}
		}
		if (drawing?.data.count)! > 0 {
			for i in 0..<(drawing?.data.count)! {
				for j in 1..<(drawing?.data[i].count)! {
					drawSingleStraightLine(context: context,
					                       currentPoint: (drawing?.data[i][j].location)!,
					                       prevPoint: (drawing?.data[i][j-1].location)!,
					                       width: widthForLine,
					                       colorRed: 0.0,
					                       colorGreen: 0.0,
					                       colorBlue: 0.0)
				}
			}
		}
	}
	
	
	func drawLines(context: CGContext, drawing: Drawing?) {
		if drawing == nil {
			return
		}
		if line.count > 0 {
			for i in 2..<(line.count) {
				drawSingleLine(context: context,
				               currentPoint: line[i].location,
				               prevPoint1: line[i-1].location,
				               prevPoint2: line[i-2].location,
				               width: widthForLine,
				               colorRed: 0.0,
				               colorGreen: 0.0,
				               colorBlue: 0.0)
			}
		}
		if (drawing?.data.count)! > 0 {
			for i in 0..<(drawing?.data.count)! {
				for j in 2..<(drawing?.data[i].count)! {
					drawSingleLine(context: context,
					               currentPoint: (drawing?.data[i][j].location)!,
					               prevPoint1: (drawing?.data[i][j-1].location)!,
					               prevPoint2: (drawing?.data[i][j-2].location)!,
					               width: widthForLine,
					               colorRed: 0.0,
					               colorGreen: 0.0,
					               colorBlue: 0.0)
				}
			}
		}
	}
	
	private func drawPressure(context: CGContext, drawing: Drawing?) {
		if drawing == nil {
			return
		}
		if line.count > 0 {
			for i in 2..<(line.count) {
				drawSingleLine(context: context,
				               currentPoint: line[i].location,
				               prevPoint1: line[i-1].location,
				               prevPoint2: line[i-2].location,
				               width: widthForPressure + line[i].force * 20.0,
				               colorRed: 1.0 / (1.0 + pow(5.0, (-line[i].force + 1))),
				               colorGreen: 0.0,
				               colorBlue: 0.0)
			}
		}
		if (drawing?.data.count)! > 0 {
			for i in 0..<(drawing?.data.count)! {
				for j in 2..<(drawing?.data[i].count)! {
					drawSingleLine(context: context,
					               currentPoint: (drawing?.data[i][j].location)!,
					               prevPoint1: (drawing?.data[i][j-1].location)!,
					               prevPoint2: (drawing?.data[i][j-2].location)!,
					               width: widthForPressure + (drawing?.data[i][j].force)! * 20.0,
					               colorRed: 1.0 / (1.0 + pow(5.0, (-(drawing?.data[i][j].force)! + 1))),
					               colorGreen: 0.0,
					               colorBlue: 0.0)
				}
			}
		}
	}
	
	override func draw(_ rect: CGRect) {
		imageTemp.image = nil
		let context = imageContextStart(image: imageTemp)
		switch drawingMode {
		case .Lines		: drawLines(context: context!, drawing: drawing)
		case .Dots		: drawDots(context: context!, drawing: drawing)
		case .Pressure	: drawPressure(context: context!, drawing: drawing)
		}
		imageContextEnd(image: imageTemp)
	}
	
	func imageContextStart(image imageView: UIImageView) -> CGContext! {
		let size = CGSize(width: self.bounds.width, height: self.bounds.height)
		UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
		let context = UIGraphicsGetCurrentContext()
		imageView.image?.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
		return context
	}
	
	func imageContextEnd(image imageView: UIImageView) {
		imageView.image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
	}
    
    func drawCircle(centerX: Double, centerY: Double, radius: Double, color: UIColor) {
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: centerX, y: centerY), radius: CGFloat(radius), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = color.cgColor
        //you can change the line width
        shapeLayer.lineWidth = 3.0
        self.layer.addSublayer(shapeLayer)
        self.setNeedsDisplay()
    }
    
    func drawDigits(digits: [[String: Any]]) {
        for digit in digits {
            let val = digit["digit"] as! Double
            let min = digit["min"] as! [String: Double]
            let minX = min["x"]
            let minY = min["y"]
            let max = digit["max"] as! [String: Double]
            let maxX = max["x"]
            let maxY = max["y"]
            //let prob = digit["probability"] as! Double
            let rect = UIBezierPath(rect: CGRect(x: minX!, y: minY!, width: maxX!-minX!, height: maxY!-minY!))
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = rect.cgPath
            //change the fill color
            shapeLayer.fillColor = UIColor.clear.cgColor
            //you can change the stroke color
            shapeLayer.strokeColor = UIColor.green.cgColor
            //you can change the line width
            shapeLayer.lineWidth = 2.0
            self.layer.addSublayer(shapeLayer)
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 21))
            label.center = CGPoint(x: (maxX! + 25), y: maxY!)
            label.textAlignment = NSTextAlignment.left
            label.text = val.description
            self.addSubview(label)
        }
        self.setNeedsDisplay()
    }
    
    
}
