//
//  DrawViewSettingsController.swift
//  Mediag
//
//  Created by Konstantin Bardõš on 18/10/16.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit

class DrawViewSettingsController: UIViewController, UIPopoverPresentationControllerDelegate {
	
	var mainView: DrawView!
	
	var valueAdvanced : Bool!
	var valueFinger : Bool!
	var valueDrawingMode : Int!
	var valueLineWidth : Float!
	
	
	@IBOutlet weak var labelAdvanced: UILabel!
	@IBOutlet weak var labelFinger: UILabel!
	@IBOutlet weak var labelDrawingMode: UILabel!
	@IBOutlet weak var labelLineWidth: UILabel!
	
	@IBOutlet weak var controlAdvanced: UISwitch!
	@IBOutlet weak var controlFinger: UISwitch!
	@IBOutlet weak var controlDrawingMode: UISegmentedControl!
	@IBOutlet weak var controlLineWidth: UISlider!
	
	
	@IBAction func controlAdvancedChanged(_ sender: UISwitch) {
		mainView.isAdvancedMode = sender.isOn
	}
	
	@IBAction func controlFingerChanged(_ sender: UISwitch) {
		mainView.isFingerInputRestricted = sender.isOn
	}
	
	@IBAction func controlDrawingModeChanged(_ sender: UISegmentedControl) {
		mainView.drawingModeIndex = sender.selectedSegmentIndex
	}
	
	@IBAction func controlLineWidthChanged(_ sender: UISlider) {
		mainView.widthScaling = CGFloat(sender.value)
		mainView.setNeedsDisplay()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		updateUI()
	}
	
	private func updateUI() {
		controlAdvanced.isOn = valueAdvanced
		controlFinger.isOn = valueFinger
		controlDrawingMode.selectedSegmentIndex = valueDrawingMode
		controlLineWidth.value = valueLineWidth
		
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		modalPresentationStyle = .popover
		popoverPresentationController?.delegate = self
		preferredContentSize = CGSize(width: 400, height: 200)
	}
}
