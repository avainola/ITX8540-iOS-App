//
//  AddPatientInfoViewController.swift
//  Mediag
//
//  Created by Artur Vainola on 07/11/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit

class AddPatientInfoViewController: UIViewController {

    var patientId: String?
    var gender: String?
    var handedness: String?
    var dateOfBirth = Date(timeIntervalSince1970: 0) {
        didSet {
            if ((dateField) != nil) {
                dateField.text = dateFormatter.string(from: dateOfBirth)
            }
        }
    }
    let dateFormatter = DateFormatter()
    var infoVC: InfoViewController?
    var persistanceManager: PersistenceManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.gender == "F" {
            self.genderControl.selectedSegmentIndex = 1
        }
        else {
            self.genderControl.selectedSegmentIndex = 0
            self.gender = "M"
        }
        
        if self.handedness == "L" {
            self.handednessControl.selectedSegmentIndex = 0
        }
        else {
            self.handednessControl.selectedSegmentIndex = 1
            self.handedness = "R"
        }
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateField.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dateFieldEditing))
        dateField.isUserInteractionEnabled = true
        dateField.addGestureRecognizer(tap)
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        self.datePickerView.isHidden = true
        dateField.text = dateFormatter.string(from: self.dateOfBirth)
    }
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var dateField: UILabel!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var genderControl: UISegmentedControl!
    @IBOutlet weak var handednessControl: UISegmentedControl!
    
    @IBAction func ganderChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.gender = "M"
        } else {
            self.gender = "F"
        }
    }
    
    @IBAction func handednessChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.handedness = "L"
        } else {
            self.handedness = "R"
        }
    }
    
    func dateFieldEditing() {
        //self.stackView.addArrangedSubview(datePickerView)
        //datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        //self.stackView.removeArrangedSubview(self.datePickerView)
        self.datePickerView.isHidden = !self.datePickerView.isHidden
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        //dateField.text = dateFormatter.string(from: sender.date)
        dateOfBirth = sender.date
    }
    
    
    @IBAction func save(_ sender: UIButton) {
        let patientInfo = ["id": self.patientId!, "dateOfBirth": dateOfBirth.description, "sex": self.gender!, "hand": self.handedness!] as [String : Any]
        persistanceManager?.addPatientInfo(info: patientInfo) { status, message, data in
            if status == 200 {
                self.infoVC?.fetchPatientInfo()
                self.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
