//
//  ResultsTableViewCell.swift
//  Mediag
//
//  Created by Artur Vainola on 13/11/2016.
//
//

import UIKit

class ResultsTableViewCell: UITableViewCell {

    @IBOutlet weak var resultNameLabel: UILabel!
    @IBOutlet weak var avgLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    var result: [String: Any]? {
        didSet {
            let label = result?["label"] as! String
            let avg = result?["avg"] as! Double
            let stdev = result?["stdev"] as! Double
            let value = result?["value"] as! Double
            
            self.resultNameLabel.text = label
            self.avgLabel.text = avg.description + " ± " + stdev.description
            self.valueLabel.text = value.description
            if value < avg - stdev || value > avg + stdev {
                self.valueLabel.textColor = .red
            }
        }
    }
    
    
    
}
