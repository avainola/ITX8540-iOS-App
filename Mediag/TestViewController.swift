//
//  ResultsViewController.swift
//  Mediag
//
//  Created by Artur Vainola on 23/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit
//import Charts

struct TestsPreloaded {
	
	static let Data = [
		["label":		"Poppelreuter test",
		 "value":		"Trace overlapping curves",
		 "fileName":	"poppelreuter",
		 "mode":		"dots"],
		
		["label":		"Sinus trace test",
		 "value":		"Trace sinus curve",
		 "fileName":	"sine_trace",
		 "mode":		"dots"],
		
		["label":		"Sinus line small test",
		 "value":		"Continue sinus curve",
		 "fileName":	"sin_line_small",
		 "mode":		"dots"],
		
		["label":		"PL curve trace test",
		 "value":		"Trace PL curve",
		 "fileName":	"pl_trace",
		 "mode":		"dots"],
		
		["label":		"PL curve copy test",
		 "value":		"Copy PL curve",
		 "fileName":	"pl_copy",
		 "mode":		"dots"],
		
		["label":		"PL curve continue test",
		 "value":		"Continue PL curve",
		 "fileName":	"pl_continue",
		 "mode":		"dots"],
		
		["label":		"Clock test",
		 "value":		"Draw a clock",
		 "fileName":	"clock",
		 "mode":		"dots"]]
	
	static func getValue(forName: String, key: String) -> String! {
		for test in self.Data {
			if test["fileName"] != nil, test["fileName"] == forName, test[key] != nil {
				return test[key]!
			}
		}
		return nil
	}
	
	static func getLabel(forName: String) -> String! {
		return getValue(forName: forName, key: "label")
	}
	
	static func getDescription(forName: String) -> String! {
		return getValue(forName: forName, key: "value")
	}
	
	static func getFilename(forIndex: Int) -> String! {
		return Data[forIndex]["fileName"]
	}
	
	static func getDescription(forIndex: Int) -> String! {
		return Data[forIndex]["value"]
	}
	
	static func getLabel(forIndex: Int) -> String! {
		return Data[forIndex]["label"]
	}
	
}

class TestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
	var testId: String!
	var patientId: String!
	var persistenceManager: PersistenceManager?
	
	var results = [[String: String]]() {
		didSet {
			tableViewLeft.reloadData()
		}
	}
	
	@IBOutlet weak var tableViewLeft: UITableView!
    
    @IBOutlet weak var tableViewRight: UITableView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableViewLeft.delegate = self
        tableViewRight.delegate = self
        tableViewLeft.dataSource = self
        tableViewRight.dataSource = self
	}
    
    override func viewWillAppear(_ animated: Bool) {
        if let selectedRow = self.tableViewLeft.indexPathForSelectedRow {
            self.tableViewLeft.deselectRow(at: selectedRow, animated: true)
        }
        else if let selectedRow = self.tableViewRight.indexPathForSelectedRow {
            self.tableViewRight.deselectRow(at: selectedRow, animated: true)
        }
    }

	override func viewDidAppear(_ animated: Bool) {
		self.tableViewLeft.flashScrollIndicators()
        self.tableViewRight.flashScrollIndicators()
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return TestsPreloaded.Data.count
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.tableViewLeft {
            return "Choose left hand test"
        }
        else  {
            return "Choose right hand test"
        }
		
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Test", for: indexPath)
		
		cell.textLabel?.text		= TestsPreloaded.getLabel(forIndex: indexPath.row)
		cell.detailTextLabel?.text	= TestsPreloaded.getDescription(forIndex: indexPath.row)
		
		return cell
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let identifier = segue.identifier {
			switch identifier {
			case "DrawL":
				if let cell = sender as? UITableViewCell,
					
					let indexPath = tableViewLeft.indexPath(for: cell),
					let drawvc = segue.destination as? DrawViewController {
					
					drawvc.patientId = self.patientId
                    drawvc.drawingHand = "L"
					drawvc.navigationItem.title = "Patient " + self.patientId + " - " + TestsPreloaded.Data[indexPath.row]["label"]! + " with left hand"
					drawvc.persistenceManager = self.persistenceManager
					let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: drawvc, action: #selector(DrawViewController.doneTapped(_:)))
					drawvc.navigationItem.setRightBarButton(doneButton, animated: true)
					drawvc.doneButton = doneButton
					let clearButton = UIBarButtonItem(barButtonSystemItem: .trash, target: drawvc, action: #selector(DrawViewController.clearTapped(_:)))
					drawvc.toolbarItems?.append(clearButton)
					drawvc.clearButton = clearButton
					
//					drawvc.patientId = self.patientId
//					drawvc.persistenceManager = self.persistenceManager
//					drawvc.navigationItem.title = "Patient: " + self.patientId
					
					drawvc.drawingType = TestsPreloaded.getFilename(forIndex: indexPath.row)

				}
				break
            case "DrawR":
                if let cell = sender as? UITableViewCell,
                    
                    let indexPath = tableViewRight.indexPath(for: cell),
                    let drawvc = segue.destination as? DrawViewController {
                    
                    drawvc.patientId = self.patientId
                    drawvc.drawingHand = "R"
                    drawvc.navigationItem.title = "Patient " + self.patientId + " - " + TestsPreloaded.Data[indexPath.row]["label"]! + " with right hand"
                    drawvc.persistenceManager = self.persistenceManager
                    let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: drawvc, action: #selector(DrawViewController.doneTapped(_:)))
                    drawvc.navigationItem.setRightBarButton(doneButton, animated: true)
                    drawvc.doneButton = doneButton
                    let clearButton = UIBarButtonItem(barButtonSystemItem: .trash, target: drawvc, action: #selector(DrawViewController.clearTapped(_:)))
                    drawvc.toolbarItems?.append(clearButton)
                    drawvc.clearButton = clearButton
                    
                    //					drawvc.patientId = self.patientId
                    //					drawvc.persistenceManager = self.persistenceManager
                    //					drawvc.navigationItem.title = "Patient: " + self.patientId
                    
                    drawvc.drawingType = TestsPreloaded.getFilename(forIndex: indexPath.row)
                    
                }
                break
			default:
				break
			}
		}
		
	}
	
}
